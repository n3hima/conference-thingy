var io = require('socket.io');
var _ = require('underscore');

var clients = [];

exports.sockets = function () {
    var sockets = io.listen(8080);
    
    sockets.on('connection', function (socket) {
	clients.push(socket);
	
	socket.on('sendMsg', function (data) {
	   if (data && data.message && data.user) {
	       _.map(clients, function (client) {
		   client.emit('recvMsg', { message: data.message, user: data.user });
	       });
	   }
	});
    });

    return function(req, res, next) {
	
    };
}
