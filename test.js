var socket = io.connect('http://localhost:8080');
socket.on('recvMsg', function (data) {
    if (data && data.message && data.user) {
	push(document.getElementsByName('chatwindow')[0], data.user + ": " + data.message);
    }
});

function submitForm () {
    var msgBox = document.message.messageText;
    var usrBox = document.message.user;
    socket.emit('sendMsg', { user: usrBox.value, message: msgBox.value });
    msgBox.value = '';
}

function push (tArea, text) {
    tArea.value = tArea.value + "\n" + text;
}
